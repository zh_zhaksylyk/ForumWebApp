﻿using Microsoft.AspNetCore.Mvc;
using ForumProject.ViewModels;
using AutoMapper;
using DAL.Interfaces;
using DAL.DTOs.Category;

namespace ForumProject.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryController(ICategoryRepository categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("/categories")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Route("/categories")]
        public IActionResult Create(CategoryCreateVM viewModel)
        {
            if (ModelState.IsValid)
            {
                var category = _mapper.Map<CategoryCreateDTO>(viewModel);

                _categoryRepository.Create(category);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Invalid login or(and) password");
            }

            return View(viewModel);
        }
    }
}