﻿using BLL.Interfaces;
using DAL.DTOs.Survey;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ForumProject.Controllers
{
    public class SurveyController : BaseController
    {
        private readonly ISurveyService _surveyService;

        public SurveyController(UserManager<User> userManager, ISurveyService surveyService):base(userManager)
        {
            _surveyService = surveyService;
        }

        public new IActionResult Index()
        {
            return View();
        }

        [Route("survey/create/{communityId}")]
        public IActionResult Create(int communityId)
        {
            var currentUserId = GetCurrentUserId();
            ViewData["CommunityId"] = communityId;
            ViewData["AuthorId"] = currentUserId;
            return View();
        }

        [HttpPost]
        [Route("survey/create/{communityId}")]
        public IActionResult Create(SurveyCreateDTO surveyCreateDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _surveyService.CreateSurvey(surveyCreateDTO);
            return CreatedAtRoute("CommunityDetails", new { id = surveyCreateDTO.CommunityId }, null);
        }

        [Route("survey/details/{surveyId}")]
        public IActionResult Details(int surveyId)
        {
            var currentUserId = GetCurrentUserId();
            var surveyWithResultsDto = _surveyService.GetSurveyWithResults(surveyId, currentUserId);
            return View(surveyWithResultsDto);
        }

        [HttpPost]
        public IActionResult Update(SurveyFillOutFormDTO form)
        {
            form.UserId = (int)GetCurrentUserId();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _surveyService.FillOutSurvey(form);
            return Ok();
        }

        [Route("survey/results/{surveyId}")]
        public IActionResult Results(int surveyId)
        {
            var currentUserId = GetCurrentUserId();
            var surveyWithResultsDto = _surveyService.GetSurveyWithResults(surveyId, currentUserId);
            return PartialView("_SurveyResultsPartial", surveyWithResultsDto);
        }
    }
}