﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ForumProject.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly UserManager<User> _userManager;

        public BaseController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        public int? GetCurrentUserId()
        {
            return _userManager.GetUserAsync(HttpContext.User).Result?.Id;
        }
    }
}