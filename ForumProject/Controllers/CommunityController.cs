﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using ForumProject.ViewModels.Post;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using DAL.Entities;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using ForumProject.ViewModels;
using ForumProject.ViewModels.Community;
using DAL.Interfaces;
using AutoMapper;
using DAL.DTOs.Community;
using DAL.DTOs.User;
using ForumProject.Utils;

namespace ForumProject.Controllers
{
    public class CommunityController : Controller
    {
        private readonly IWebHostEnvironment _appEnvironment;

        private readonly ICommunityRepository _communityRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;

        private readonly UserManager<User> _userManager;

        public CommunityController(
            ICategoryRepository categoryRepository,
            UserManager<User> userManager,
            IWebHostEnvironment appEnvironment, 
            IPostRepository postRepository,
            IMapper mapper,
            ICommunityRepository communityRepository)
        {
            _categoryRepository = categoryRepository;
            _userManager = userManager;
            _appEnvironment = appEnvironment;
            _postRepository = postRepository;
            _mapper = mapper;
            _communityRepository = communityRepository;
            _postRepository = postRepository;
        }

        [HttpGet]
        [Route("/communities/{categoryId?}")]
        public IActionResult Index(int? categoryId)
        {
            var categories = _categoryRepository.GetAll()
                .Select(category => _mapper.Map<CategoryVM>(category))
                .ToList();

            var communities = _communityRepository.GetAll()
                .Select(community => _mapper.Map<CommunityVM>(community));

            categories.Insert(0, new CategoryVM { Id = 0, Name = "All" });

            var viewModel = new CommunityIndexViewModel { Categories = categories, Communities = communities };

            if (categoryId != null && categoryId > 0)
            {
                viewModel.Communities = communities.Where(c => c.CategoryId == categoryId);
            }

            return View(viewModel);
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        [Route("/community/create")]
        public IActionResult Create()
        {
            var categories = _categoryRepository.GetAll()
                .Select(category => _mapper.Map<CategoryVM>(category));

            ViewData["Categories"] = new SelectList(categories, "Id", "Name");

            return View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [Route("/community/create")]
        public async Task<IActionResult> Create(CommunityCreateVM viewModel)
        {
            if (ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);               
                var user = await _userManager.FindByIdAsync(userId);

                var community = _mapper.Map<CommunityDTO>(viewModel);
                community.Creator = _mapper.Map<UserDTO>(user);
                community.PhotoPath = ImageUtil.Upload(_appEnvironment, viewModel.Photo, "communities");

                _communityRepository.Add(community);
                return RedirectToAction("Details", "Community", new { id = community.Id });
            }

            return View(viewModel);
        }

        [HttpGet]
        [Route("/community/{id}", Name = "CommunityDetails")]
        public IActionResult Details(int id)
        {
            var community = _communityRepository.GetById(id);
            var communityVM = _mapper.Map<CommunityVM>(community);

            var posts = _postRepository.GetPostsByCommunity(community.Id)
                .Select(post => _mapper.Map<PostVM>(post))
                .ToList();

            posts.ForEach(post => post.Community = communityVM);
                              
            return View(new CommunityDetailsVM { Community = communityVM, Posts = posts});
        }
    }
}