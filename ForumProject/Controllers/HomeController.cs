﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ForumProject.Models;
using ForumProject.ViewModels;
using Microsoft.AspNetCore.Authorization;
using DAL.Interfaces;
using ForumProject.ViewModels.Post;

namespace ForumProject.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly IPostRepository _postRepository;

        public HomeController(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var posts = _postRepository.GetAll();
            
            var postViewModels = posts.Select(post => new PostVM
                {
                    Id = post.Id,
                    Title = post.Title,
                    AuthorId = post.Author.Id,
                    AuthorName = post.Author.DisplayName,
                    Created = post.Created.ToString(),
                    CommentsCount = post.Comments.Count(),
                    Community = new CommunityVM
                    {
                        Id = post.Community.Id,
                        Title = post.Community.Title,
                        Description = post.Community.Description,
                        PhotoPath = post.Community.PhotoPath,
                        CategoryId = post.Community.CategoryId
                    }
                });

            var viewModel = new HomeIndexViewModel
            {
                Posts = postViewModels
            };

            return View(viewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
