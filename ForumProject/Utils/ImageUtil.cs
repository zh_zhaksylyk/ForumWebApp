﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace ForumProject.Utils
{
    public static class ImageUtil
    {
        public static string Upload(IWebHostEnvironment appEnvironment, IFormFile file, string folderName)
        {
            string uploadsFolder = Path.Combine(appEnvironment.WebRootPath, "images", folderName);
            string uniqueFileName = "default.jpg";

            if (file != null)
            {
                uniqueFileName = Path.GetRandomFileName();
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                file.CopyTo(new FileStream(filePath, FileMode.Create));
            }

            return uniqueFileName; 
        }
    }
}
