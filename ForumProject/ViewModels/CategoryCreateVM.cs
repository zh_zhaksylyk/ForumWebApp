﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ForumProject.ViewModels
{
    public class CategoryCreateVM
    {
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }
    }
}