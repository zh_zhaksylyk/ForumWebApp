﻿using System.Collections.Generic;
using ForumProject.ViewModels.Post;

namespace ForumProject.ViewModels.Community
{
    public class CommunityDetailsVM
    {
        public CommunityVM Community { get; set; }
        public IEnumerable<PostVM> Posts { get; set; }
    }
}
