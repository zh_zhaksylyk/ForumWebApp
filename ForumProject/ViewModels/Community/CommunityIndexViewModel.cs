﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumProject.ViewModels
{
    public class CommunityIndexViewModel
    {
        public IEnumerable<CategoryVM> Categories { get; set; }
        public IEnumerable<CommunityVM> Communities { get; set; }
    }
}
