﻿var questionCounter = 0;
var optionCounter = 0;
function addQuestion() {
	questionCounter++;
	var index = questionCounter;
	var question = $("<div>").addClass("question");
	// question text
	var formField = $("<div>").addClass("form-field");
	$("<label>Question</label>").appendTo(formField);
	$("<input>").attr({ type: "hidden", name: "Questions.Index", value: index }).appendTo(formField);
	$("<input>").attr({ type: "text", name: "Questions[" + index + "].Text"}).addClass("field").appendTo(formField);
	// option-list
	var optionList = $("<div>").addClass("option-list");
	var option = $("<div>").addClass("option form-field");
	optionCounter++;
	$("<input>").attr({ type: "hidden", name: "Questions[" + index + "].Answers.Index", value: optionCounter }).appendTo(option);
	$("<input>").attr({ type: "text", placeholder: "Option", name: "Questions[" + index + "]" + ".Answers[" + optionCounter + "].Text" })
		.addClass("field").appendTo(option);
	option.appendTo(optionList);
	//add-option
	var addOptionEl = $("<button>Add option</button>").attr("type", "button").addClass("add-option");
	addOptionEl.on("click", { questionIndex: index }, addOption).appendTo(optionList);
	// delete-question 
	var tooltip = $("<div>").addClass("tooltip-wrapper");
	var deleteQuestionEl = $("<span>").addClass("delete-question far fa-trash-alt");
	deleteQuestionEl.on("click", deleteQuestion).appendTo(tooltip);
	$("<span>Delete</span>").addClass("tooltiptext").appendTo(tooltip);

	question.append(formField, optionList, tooltip);
	question.insertBefore($("#save"));
}

function deleteQuestion(e) {
	$(e.target).closest("div.question").remove();
}

function deleteOption(e) {
	$(e.target).closest("div.option").remove();
}

function addOption(e) {
	optionCounter++;
	var questionIndex = (e.data == null) ? 0 : e.data.questionIndex;
	var newOption = $("<div>").addClass("option form-field");
	$('<input type="hidden">').attr({
		name: "Questions[" + questionIndex + "].Answers.Index",
		value: optionCounter
	}).appendTo(newOption);
	$('<input type="text">').attr({
		placeholder: "Option",
		name: "Questions[" + questionIndex + "].Answers[" + optionCounter + "].Text"
	}).addClass("field").appendTo(newOption);
	//with delete functionality
	var deleteOptionEl = $("<span>x</span>").attr({ "aria-hidden": "true", "aria-label": "Close" }).addClass("remove-option close");
	deleteOptionEl.on("click", deleteOption).appendTo(newOption);

	newOption.insertBefore($(e.target));
}

$(document).ready(function () {
	$(".add-question").on("click", addQuestion);
	addQuestion();

	$("#surveyForm").on("submit", function (e) {
		$(".field").each(function () {
			$(this).rules('add', {
				required: true
			});
		});
	});

	$("#surveyForm").removeData("validator");
	$("#surveyForm").removeData("unobtrusiveValidation");

	$("#surveyForm").validate({
		errorClass: "text-danger",
		errorPlacement: function (error, element) {
			error.insertAfter(element.closest("div"));
		}
	});

	$("#surveyForm").on("submit", function (e) {
		e.preventDefault();

		var formData = new FormData(e.target);
		var url = $("#surveyForm").attr("action");

		if ($("#surveyForm").valid()) {
			$("#save").attr("disabled", true);

			$.ajax({
				method: "POST",
				url: url,
				data: formData,
				processData: false,
				contentType: false,
				statusCode: {
					201: function (data, statusText, xhr) {
						$(".popup").show();
						window.setTimeout(function () {
							window.location.replace(xhr.getResponseHeader("Location"));
						}, 3000);
					},
					400: function (xhr, textStatus, error) {
						var message = JSON.parse(xhr.responseText);
						var text = "Post is not saved.\n";
						for (let [key, value] of Object.entries(message)) {
							text += `${key}\n${value}\n`;
						}
						alert(text);
						$("#save").attr("disabled", false);
					}
				}
			});
		}
	});
});