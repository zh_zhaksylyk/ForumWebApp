﻿$(document).ready(function () {
    $("#surveyVoteForm").on("submit", function (e) {
        $(".answer input[type=radio]:first-of-type").each(function () {
            var groupName = $(this).attr("name");
            $('input[name="' + groupName + '"]').rules('add',
                {
                    required: true,
                    messages: {
                        required: "The answer is required to the question"
                    }
                });
        });
    });

    $("#surveyVoteForm").validate({
        errorClass: "text-danger",
        errorPlacement: function (error, element) {
            error.insertBefore(element.closest("div.question-with-options"));
        }
    });

	$("#surveyVoteForm").on("submit", function (e) {
		e.preventDefault();

		var formData = new FormData(e.target);
		var url = $("#surveyVoteForm").attr("action");

		if ($("#surveyVoteForm").valid()) {
			$("#vote").attr("disabled", true);

			$.ajax({
				method: "POST",
				url: url,
				data: formData,
				processData: false,
				contentType: false,
				statusCode: {
					200: function (data, statusText, xhr) {
						$(".popup").show();
						$(".popup").delay(2000).fadeOut(500);
					},
					400: function (xhr, textStatus, error) {
						var message = JSON.parse(xhr.responseText);
						var text = "Post is not saved.\n";
						for (let [key, value] of Object.entries(message)) {
							text += `${key}\n${value}\n`;
						}
						alert(text);
						$("#vote").attr("disabled", false);
					}
				}
			}).done(function () {
				var surveyId = $("#surveyId").val();
				var urlToDetails = `/survey/results/${surveyId}`;
				$.get(urlToDetails, function (result) {
					$("#surveyVoteForm").replaceWith(result);
				});
			});
		}
	});
});