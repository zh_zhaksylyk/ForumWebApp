﻿using AutoMapper;
using DAL.DTOs.Post;
using ForumProject.ViewModels.Post;
using System.Linq;

namespace ForumProject.Mappings
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<PostDTO, PostVM>()
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(src => src.Author.DisplayName))
                .ForMember(dest => dest.SurveyId, opt => opt.MapFrom(src => src.Survey.Id))
                .ForMember(dest => dest.CommentsCount, opt => opt.MapFrom(src => src.Comments.Count()))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.Created.ToString()));
        }
    }
}
