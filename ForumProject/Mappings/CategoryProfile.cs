﻿using AutoMapper;
using DAL.DTOs.Category;
using ForumProject.ViewModels;

namespace ForumProject.Mappings
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<CategoryCreateVM, CategoryCreateDTO>();
            CreateMap<CategoryGetDTO, CategoryVM>();
            CreateMap<CategoryGetDTO, CategoryVM>();
        }
    }
}