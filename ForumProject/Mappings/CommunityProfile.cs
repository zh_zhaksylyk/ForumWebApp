﻿using AutoMapper;
using DAL.DTOs.Community;
using ForumProject.ViewModels;

namespace ForumProject.Mappings
{
    public class CommunityProfile : Profile
    {
        public CommunityProfile()
        {
            CreateMap<CommunityDTO, CommunityVM>();
            CreateMap<CommunityCreateVM, CommunityDTO>();
        }
    }
}
