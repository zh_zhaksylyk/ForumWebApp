using NUnit.Framework;
using DAL.Repositories;
using DAL.Entities;
using DAL.DTOs.Survey;
using DAL.Data;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using BLL.Mappings;

namespace ForumProjectTest
{
    [TestFixture]
    public class SurveyRepositoryTests
    {
        private IMapper _mapper;
        private ForumContext _context;

        [SetUp]
        public void Setup()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<SurveyProfile>();
            });

            _mapper = config.CreateMapper();

            _context = ConnectionFactory.CreateDbContext();


            var author = new User { Id = 1, DisplayName = "author", UserName = "author" };
            var community = new Community
            {
                Id = 1,
                Title = "swe",
                Description = "swe questions",
                Category = new Category { Id = 1, Name = "Software engineering"}
            };

            var post = new Post
            {
                Id = 1,
                Title = "Web framework",
                Content = "Choosing the best web framework",
                Author = author,
                Community = community
            };

            var survey = new Survey
            {
                Id = 1,
                Post = post,
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 1,
                        Text = "Which web framework are you using?",
                        Answers = new List<Answer>
                        {
                            new Answer { Id = 1, Text = "ASP.Net" },
                            new Answer { Id = 2, Text = "Django" },
                            new Answer { Id = 3, Text = "Spring" }
                        }
                    }
                },

                SurveyParticipants = new List<SurveyParticipant>()
            };

            _context.Add(survey);
            _context.SaveChanges();

        }

        [Test]
        public void Create_Writes_To_Database()
        {
            var surveyDto = new SurveyCreateDTO
            {
                AuthorId = 1,
                Title = "Unit testing framework",
                Description = "xUnit, nUnit, ms test",
                Questions = new List<SurveyCreateDTO.Question>
                {
                    new SurveyCreateDTO.Question
                    {
                        Text = "Choose unit testing framework",
                        Answers = new List<SurveyCreateDTO.Answer>
                        {
                            new SurveyCreateDTO.Answer{ Text = "xUnit"},
                            new SurveyCreateDTO.Answer{ Text = "nUnit"},
                            new SurveyCreateDTO.Answer{ Text = "mstest"}
                        }
                    }
                },
                CommunityId = 1
            };

            var surveyRepository = new SurveyRepository(_context);
            surveyRepository.Create(surveyDto);
            _context.SaveChanges();

            _context.Surveys.Should().Contain(s => s.Post.Title == "Unit testing framework");
            _context.Questions.Should().Contain(q => q.Text == "Choose unit testing framework")
                .Which.Answers.Count.Should().Be(3);
            _context.Answers.Should().Contain(a => a.Text == "xUnit");
        }

        [Test]
        public void GetWithResults_Returns_SurveyDto_With_Related_Data()
        {
            int surveyId = 1;
            var survey = _context.Surveys.SingleOrDefault(s => s.Id == surveyId);

            var surveyRepository = new SurveyRepository(_context, _mapper);
            var resultDto = surveyRepository.GetWithResults(surveyId);


            resultDto.Should().BeOfType<SurveyWithResultsDTO>()
                .Which.SurveyId.Should().Be(survey.Id);
            resultDto.Title.Should().Be(survey.Post.Title);
            resultDto.Description.Should().Be(survey.Post.Content);
            resultDto.Questions.Should().HaveCount(survey.Questions.Count());

            var question = _context.Questions.First(q => q.SurveyId == survey.Id);
            resultDto.Questions.Should().Contain(q => q.QuestionId == question.Id && 
                                                 q.Answers.Count() == question.Answers.Count());

            resultDto.ParticipantCount.Should().Be(survey.SurveyParticipants.Count());
        }

        [TearDown]
        public void TearDown()
        {
            _context.Database.CloseConnection();
            _context.Dispose();
        }
    }
}