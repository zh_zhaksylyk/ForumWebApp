﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using DAL.Data;

namespace ForumProjectTest
{
    public static class ConnectionFactory
    {
        public static ForumContext CreateDbContext()
        {
            SqliteConnection connection = new SqliteConnection("Data Source=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<ForumContext>()
            .UseSqlite(connection)
            .Options;

            var context = new ForumContext(options);

            context.Database.EnsureCreated();
            return context;
        }
    }
}
