﻿using NUnit.Framework;
using Moq;
using BLL.Services;
using AutoMapper;
using DAL.Interfaces;
using DAL.DTOs.Survey;
using System.Collections.Generic;
using DAL.Entities;
using System.Linq;
using FluentAssertions;
using BLL.Exceptions;
using BLL.Mappings;
using BLL.DTOs;

namespace ForumProjectTest
{
    public class SurveyServiceTests
    {
        private IMapper _mapper;
        private Mock<ISurveyRepository> _mockSurveyRepository;
        private Mock<ISurveyResultRepository> _mockSurveyResultsRepository;

        [SetUp]
        public void SetUp()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.AddProfile<SurveyProfile>();
            });

            _mapper = config.CreateMapper();

            var surveyWithResultsDto = new SurveyWithResultsDTO
            {
                SurveyId = 1,
                Title = "A&DS sources to learn",
                Description = "A&DS sources to learn",
                ParticipantCount = 9,
                Questions = new List<SurveyWithResultsDTO.Question>
                {
                    new SurveyWithResultsDTO.Question
                    {
                        QuestionId = 1,
                        Text = "Platrform to learn A&DS",
                        Answers = new List<SurveyWithResultsDTO.Answer>
                        {
                            new SurveyWithResultsDTO.Answer{ Text = "Hackerrank", VoteCount = 3},
                            new SurveyWithResultsDTO.Answer{ Text = "Leetcode", VoteCount = 5},
                            new SurveyWithResultsDTO.Answer{ Text = "Codewars", VoteCount = 1}
                        }
                    }
                }
            };

            var surveyParticipants = new List<SurveyParticipant>
            {
                new SurveyParticipant{ SurveyId = 1, UserId = 1}
            };

            _mockSurveyRepository = new Mock<ISurveyRepository>();
            _mockSurveyRepository.Setup(m => m.GetWithResults(It.IsAny<int>()))
                .Returns((int surveyId) => surveyWithResultsDto);

            _mockSurveyResultsRepository = new Mock<ISurveyResultRepository>();
            _mockSurveyResultsRepository.Setup(m => m.DidUserParticipate(It.IsAny<int>(), It.IsAny<int>()))
                .Returns((int surveyId, int userId) => surveyParticipants.Any(sp => sp.SurveyId == surveyId && sp.UserId == userId));
        }

        [Test]
        public void GetSurveyWithResults_CalculatesVotePercentage()
        {
            int surveyId = 1, userId = 1, questionId = 1;

            var surveyService = new SurveyService(_mockSurveyRepository.Object, _mockSurveyResultsRepository.Object, _mapper);

            var result = surveyService.GetSurveyWithResults(surveyId, userId);
            var answers = result.Questions.Single(q => q.QuestionId == questionId).Answers;

            answers.Should().Contain(a => a.Text == "Hackerrank")
                .Which.VotePercentage.Should().Be(33.33);
            answers.Should().Contain(a => a.Text == "Leetcode")
                .Which.VotePercentage.Should().Be(55.56);
            answers.Should().Contain(a => a.Text == "Codewars")
                .Which.VotePercentage.Should().Be(11.11);
        }

        [Test]
        public void GetSurveyWithResults_CalculatesVotePercentage_WhenNoParticipants()
        {
            int surveyId = 2, userId = 2, questionId = 2;

            var surveyWithResultsDto = new SurveyWithResultsDTO
            {
                SurveyId = surveyId,
                Title = "A&DS sources to learn",
                Description = "A&DS sources to learn",
                ParticipantCount = 0,
                Questions = new List<SurveyWithResultsDTO.Question>
                {
                    new SurveyWithResultsDTO.Question
                    {
                        QuestionId = 2,
                        Text = "Platrform to learn A&DS",
                        Answers = new List<SurveyWithResultsDTO.Answer>
                        {
                            new SurveyWithResultsDTO.Answer{ Text = "Hackerrank", VoteCount = 0},
                            new SurveyWithResultsDTO.Answer{ Text = "Leetcode", VoteCount = 0},
                            new SurveyWithResultsDTO.Answer{ Text = "Codewars", VoteCount = 0}
                        }
                    }
                }
            };

            var mockSurveyRepository = new Mock<ISurveyRepository>();
            mockSurveyRepository
                .Setup(m => m.GetWithResults(It.IsAny<int>()))
                .Returns((int x) => surveyWithResultsDto);

            var surveyService = new SurveyService(mockSurveyRepository.Object, _mockSurveyResultsRepository.Object, _mapper);

            var result = surveyService.GetSurveyWithResults(surveyId, userId);
            var answers = result.Questions.Single(q => q.QuestionId == questionId).Answers;

            answers.Should().Contain(a => a.Text == "Hackerrank")
                .Which.VotePercentage.Should().Be(0);
            answers.Should().Contain(a => a.Text == "Leetcode")
                .Which.VotePercentage.Should().Be(0);
            answers.Should().Contain(a => a.Text == "Codewars")
                .Which.VotePercentage.Should().Be(0);
        }

        [Test]
        public void GetSurveyWithResults_Returns_SurveyWithResultsForUserDto()
        {
            int surveyId = 1, userId = 1;

            var surveyService = new SurveyService(_mockSurveyRepository.Object, _mockSurveyResultsRepository.Object, _mapper);
            var result = surveyService.GetSurveyWithResults(surveyId, userId);

            result.Should().BeOfType<SurveyWithResultsForUserDTO>()
                .Which.SurveyId.Should().Be(surveyId);
            result.CanUserParticipate.Should().BeFalse();
        }

        [Test]
        public void FillOutSurvey_Participant_Throws_Exception()
        {
            int surveyId = 1, userId = 1;
            var form = new SurveyFillOutFormDTO
            {
                SurveyId = surveyId,
                UserId = userId,
                Answers = new List<SurveyFillOutFormDTO.Answer>
                {
                    new SurveyFillOutFormDTO.Answer{ QuestionId = 1, AnswerId = 1}
                }
            };

            var surveyService = new SurveyService(_mockSurveyRepository.Object, _mockSurveyResultsRepository.Object, _mapper);

            surveyService.Invoking(s => s.FillOutSurvey(form))
                .Should().Throw<InvalidSurveyFormException>();
        }

        [Test]
        public void FillOutSurvey_NotParticipant_NotAllAnswers_ThrowsException()
        {
            int surveyId = 1, userId = 2;
            var form = new SurveyFillOutFormDTO
            {
                SurveyId = surveyId,
                UserId = userId,
                Answers = new List<SurveyFillOutFormDTO.Answer>()
            };

            var surveyService = new SurveyService(_mockSurveyRepository.Object, _mockSurveyResultsRepository.Object, _mapper);
            surveyService.Invoking(s => s.FillOutSurvey(form))
                .Should().Throw<InvalidSurveyFormException>();
        }


        [TearDown]
        public void TearDown()
        {

        }
    }
}
