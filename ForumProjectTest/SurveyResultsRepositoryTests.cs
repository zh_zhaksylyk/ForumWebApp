﻿using DAL.Data;
using DAL.Entities;
using DAL.Repositories;
using DAL.DTOs.Survey;
using NUnit.Framework;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;

namespace ForumProjectTest
{
    [TestFixture]
    public class SurveyResultsRepositoryTests
    {
        private ForumContext _context;

        [SetUp]
        public void SetUp()
        {
            _context = ConnectionFactory.CreateDbContext();

            var author = new User { Id = 1, UserName = "author", DisplayName = "author" };
            var participant = new User { Id = 2, UserName = "_jennie", DisplayName = "jennie" };
            var user = new User { Id = 3, UserName = "_sunny", DisplayName = "sunny" };

            var category = new Category { Id = 1, Name = "Software engineering" };
            var community = new Community
            {
                Id = 1,
                Title = "swe",
                Description = "swe questions",
                Category = category
            };
            var post = new Post { Id = 1, Title = "OOP priniciples", Content = "OOP design priniciples", Author = author,
            Community = community};

            var survey = new Survey
            {
                Id = 1,
                Post = post,
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = 1,
                        Text = "Which principle is the most difficult to understand ?",
                        Answers = new List<Answer>
                        {
                            new Answer { Id = 1, Text = "Encapsulation" },
                            new Answer { Id = 2, Text = "Abstration" },
                            new Answer { Id = 3, Text = "Polymorphism" },
                            new Answer { Id = 4, Text = "Inheritance" }
                        }
                    }
                }
            };

            _context.Add(survey);
            _context.Add(user);
            _context.SurveyParticipants.Add(new SurveyParticipant { Survey = survey, User = participant });

            _context.SaveChanges();
        }

        [Test]
        public void DidUserParticipate_ReturnsTrue()
        {
            int surveyId = 1;
            int userId = 2;

            var surveyResultsRepository = new SurveyResultsRepository(_context);
            var result = surveyResultsRepository.DidUserParticipate(surveyId, userId);

            result.Should().BeTrue();
        }

        [Test]
        public void DidUserParticipate_ReturnsFalse()
        {
            int surveyId = 1;
            int userId = 3;

            var surveyResultsRepository = new SurveyResultsRepository(_context);
            var result = surveyResultsRepository.DidUserParticipate(surveyId, userId);

            result.Should().BeFalse();
        }

        [Test]
        public void Update_AddsUserToParticipants_IncreasesVoteCounts()
        {
            int surveyId = 1;
            int userId = 3;

            var form = new SurveyFillOutFormDTO
            {
                SurveyId = surveyId,
                UserId = userId,
                Answers = new List<SurveyFillOutFormDTO.Answer>
                {
                    new SurveyFillOutFormDTO.Answer{ QuestionId = 1, AnswerId = 3}
                }
            };

            var surveyResultsRepository = new SurveyResultsRepository(_context);
            surveyResultsRepository.Update(form);
            _context.SaveChanges();

            _context.SurveyParticipants.Should().Contain(sp => sp.SurveyId == surveyId && sp.UserId == userId);
            _context.Answers.Should().Contain(a => a.Id == 3 && a.VoteCount == 1);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Database.CloseConnection();
            _context.Dispose();
        }
    }
}
