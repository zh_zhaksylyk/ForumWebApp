﻿using DAL.DTOs.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs.Comment
{
    public class CommentDTO : BaseDTO
    {
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }
    }
}