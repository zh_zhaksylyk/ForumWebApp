﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.DTOs.Survey
{

    public class SurveyCreateDTO
    {
        public class Question
        {
            [Required]
            public string Text { get; set; }
            public IEnumerable<Answer> Answers { get; set; }
        }

        public class Answer
        {
            [Required]
            public string Text { get; set; }
        }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Title { get; set; }
        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        public IEnumerable<Question> Questions { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public int CommunityId { get; set; }
    }
}
