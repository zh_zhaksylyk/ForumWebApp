﻿using System.Collections.Generic;

namespace DAL.DTOs.Survey
{
    public class SurveyWithResultsDTO
    {
        public class Question
        {
            public int QuestionId { get; set; }
            public string Text { get; set; }
            public IEnumerable<Answer> Answers { get; set; }
        }
        public class Answer
        {
            public int AnswerId { get; set; }
            public string Text { get; set; }
            public int VoteCount { get; set; }
            public double VotePercentage { get; set; }
        }
        public int SurveyId { get; set; }
        public int AuthorId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ParticipantCount { get; set; }
        public IEnumerable<Question> Questions { get; set; }
    }
}
