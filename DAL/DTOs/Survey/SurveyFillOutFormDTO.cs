﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs.Survey
{
    public class SurveyFillOutFormDTO
    {
        public class Answer
        {
            public int QuestionId { get; set; }
            public int AnswerId { get; set; }
        }

        public int SurveyId { get; set; }
        public int UserId { get; set; }
        public IEnumerable<Answer> Answers { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}