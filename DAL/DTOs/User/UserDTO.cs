﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs.User
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
    }
}
