﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs
{
    public class SimpleEntityDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}