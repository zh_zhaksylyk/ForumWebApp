﻿using DAL.DTOs.Category;
using DAL.DTOs.Post;
using DAL.DTOs.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs.Community
{
    public class CommunityDTO : BaseDTO
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public string PhotoPath { get; set; }
        public UserDTO Creator { get; set; }
        public int CategoryId { get; set; }
        public IEnumerable<PostDTO> Posts { get; set; }
    }
}
