﻿using DAL.DTOs.Community;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs.Category
{
    public class CategoryGetDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<SimpleEntityDTO> Communities { get; set; }
    }
}