﻿using DAL.DTOs.Community;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs.Category
{
    public class CategoryCreateDTO
    {
        public string Name { get; set; }
    }
}