﻿using DAL.DTOs.Comment;
using DAL.DTOs.Community;
using DAL.DTOs.Survey;
using DAL.DTOs.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DTOs.Post
{
    public class PostDTO : BaseDTO
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsEditedByModerator { get; set; }
        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }
        public IEnumerable<CommentDTO> Comments { get; set; }
        public SimpleEntityDTO Survey { get; set; }
    }
}
