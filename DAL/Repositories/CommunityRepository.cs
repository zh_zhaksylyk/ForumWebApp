﻿using DAL.Interfaces;
using DAL.Entities;
using DAL.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AutoMapper;
using DAL.DTOs.Community;

namespace DAL.Repositories
{
    public class CommunityRepository : ICommunityRepository
    {
        private readonly ForumContext _context;
        private readonly IMapper _mapper;

        public CommunityRepository(ForumContext context, IMapper mapper) 
        {
            _context = context;
            _mapper = mapper;
        }

        public void Add(CommunityDTO community)
        {
            _context.Add(_mapper.Map<Community>(community));
            _context.SaveChanges();
        }

        public IEnumerable<CommunityDTO> GetAll()
        {
            var result = _context.Communities
                .Include(community => community.Posts)
                .ToList()
                .Select(community => _mapper.Map<CommunityDTO>(community));

            return result;
        }

        public CommunityDTO GetById(int id)
        {
            var community = _context.Communities.Where(c => c.Id == id)
                .Include(c => c.Posts)
                    .ThenInclude(p => p.Author)
                .Include(c => c.Posts)
                    .ThenInclude(p => p.Comments)
                        .ThenInclude(comment => comment.Author)
                .FirstOrDefault();

            return _mapper.Map<CommunityDTO>(community);
        }
    }
}
