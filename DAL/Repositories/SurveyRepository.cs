﻿using DAL.Interfaces;
using DAL.Data;
using DAL.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Collections.Generic;
using DAL.DTOs.Survey;

namespace DAL.Repositories
{
    public class SurveyRepository : ISurveyRepository
    {
        private readonly ForumContext _context;
        private readonly IMapper _mapper;

        public SurveyRepository(ForumContext context)
        {
            _context = context;
        }

        public SurveyRepository(ForumContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Create(SurveyCreateDTO surveyDto)
        {
            var survey = new Survey
            {
                Post = new Post
                {
                    Title = surveyDto.Title,
                    Content = surveyDto.Description,
                    AuthorId = surveyDto.AuthorId,
                    CommunityId = surveyDto.CommunityId
                },
                Questions = surveyDto.Questions.Select(question => new Question
                {
                    Text = question.Text,
                    Answers = question.Answers.Select(answer => new Answer
                    {
                        Text = answer.Text
                    }).ToList()
                }).ToList(),

                SurveyParticipants = new List<SurveyParticipant>()
            };

            _context.Surveys.Add(survey);
            _context.SaveChanges();
        }

        public SurveyWithResultsDTO GetWithResults(int surveyId)
        {
            var survey = _context.Surveys.Include(s => s.Post).Include(s => s.Questions).ThenInclude(q => q.Answers)
                 .Include(s => s.SurveyParticipants).SingleOrDefault(s => s.Id == surveyId);
                
            SurveyWithResultsDTO surveyDto = _mapper.Map<SurveyWithResultsDTO>(survey);

            return surveyDto;
        }
    }
}