﻿using DAL.Interfaces;
using DAL.Entities;
using DAL.Data;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using DAL.DTOs.Post;

namespace DAL.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly ForumContext _context;
        private readonly IMapper _mapper;

        public PostRepository(ForumContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Add(Post post)
        {
            _context.Add(post);
            _context.SaveChanges();
        }

        public void Delete(Post post)
        {
            _context.Posts.Remove(post);
            _context.SaveChanges();
        }

        public IEnumerable<Post> GetAll()
        {
            var result = _context.Posts
                .Include(p => p.Community)
                .Include(p => p.Author)
                .Include(p => p.Comments)
                .ToList();

            return result;
        }

        public Post GetById(int id)
        {
            var post = _context.Posts.Where(p => p.Id == id)
                .Include(p => p.Author)
                .Include(p => p.Comments)
                    .ThenInclude(c => c.Author)
                .Include(p => p.Community)
                .FirstOrDefault();

            return post;
        }

        public IEnumerable<PostDTO> GetPostsByCommunity(int id)
        {
            var result = _context.Posts
                .Include(p => p.Survey)
                .Include(p => p.Author)
                .Where(p => p.CommunityId == id)
                .ToList()
                .Select(post => _mapper.Map<PostDTO>(post))
                .ToList();

            return result;
        }

        public void Update(Post post)
        {
            _context.Posts.Update(post);
            _context.SaveChanges();
        }
    }
}
