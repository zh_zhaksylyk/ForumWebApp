﻿using DAL.Interfaces;
using DAL.Entities;
using DAL.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DAL.DTOs.Category;
using AutoMapper;

namespace DAL.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ForumContext _context;
        private readonly IMapper _mapper;

        public CategoryRepository(ForumContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Create(CategoryCreateDTO category)
        {
            var categoryEntity = _mapper.Map<Category>(category);
            _context.Categories.Add(categoryEntity);
            _context.SaveChanges();
        }

        public IEnumerable<CategoryGetDTO> GetAll()
        {
            var result = _context.Categories
                .Include(category => category.Communities)
                .ToList()
                .Select(category => _mapper.Map<CategoryGetDTO>(category))
                .ToList();

            return result;
        }

        public CategoryGetDTO GetById(int id)
        {
            var category = _context.Categories.Find(id);
            return _mapper.Map<CategoryGetDTO>(category);
        }
    }
}