﻿using DAL.Interfaces;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DAL.Entities;
using DAL.DTOs.Survey;

namespace DAL.Repositories
{
    public class SurveyResultsRepository : ISurveyResultRepository
    {
        private readonly ForumContext _context;

        public SurveyResultsRepository(ForumContext context)
        {
            _context = context;
        }

        public bool DidUserParticipate(int surveyId, int userId)
        { 
            return _context.SurveyParticipants.Any(sp => sp.SurveyId == surveyId && sp.UserId == userId);
        }

        public void Update(SurveyFillOutFormDTO form)
        {
            var survey = _context.Surveys.Include(s => s.Questions).ThenInclude(q => q.Answers)
                         .Include(s => s.SurveyParticipants).SingleOrDefault(s => s.Id == form.SurveyId);

            var answers = _context.Answers.Include(answer => answer.Question)
                                          .Where(answer => answer.Question.SurveyId == form.SurveyId)
                                          .ToList();

            survey.SurveyParticipants.Add(new SurveyParticipant
            {
                SurveyId = form.SurveyId,
                UserId = form.UserId
            });

            foreach (var formAnswer in form.Answers)
            {
                answers.SingleOrDefault(answer => answer.Id == formAnswer.AnswerId).VoteCount++;
            }

            _context.SaveChanges();
        }
    }
}
