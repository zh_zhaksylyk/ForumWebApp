﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.DTOs.Post;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IPostRepository
    {
        IEnumerable<Post> GetAll();
        Post GetById(int id);
        void Add(Post post);
        void Delete(Post post);
        void Update(Post post);

        IEnumerable<PostDTO> GetPostsByCommunity(int id);
    }
}
