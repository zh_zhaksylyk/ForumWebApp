﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.DTOs.Category;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<CategoryGetDTO> GetAll();
        CategoryGetDTO GetById(int id);
        void Create(CategoryCreateDTO category);
    }
}