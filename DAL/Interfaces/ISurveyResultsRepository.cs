﻿using DAL.DTOs.Survey;

namespace DAL.Interfaces
{

    public interface ISurveyResultRepository
    {
        void Update(SurveyFillOutFormDTO form);
        bool DidUserParticipate(int surveyId, int userId);
    }
}
