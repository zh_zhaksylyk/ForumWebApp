﻿using DAL.DTOs.Survey;

namespace DAL.Interfaces
{
    public interface ISurveyRepository
    {
        void Create(SurveyCreateDTO surveyDTO);
        SurveyWithResultsDTO GetWithResults(int surveyId);
    }
}