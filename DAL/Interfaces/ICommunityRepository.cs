﻿using System.Collections.Generic;
using DAL.DTOs.Community;

namespace DAL.Interfaces
{
    public interface ICommunityRepository
    {
        IEnumerable<CommunityDTO> GetAll();
        CommunityDTO GetById(int id);
        void Add(CommunityDTO community);
    }
}
