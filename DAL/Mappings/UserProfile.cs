﻿using AutoMapper;
using DAL.DTOs.User;
using DAL.Entities;

namespace DAL.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
        }
    }
}
