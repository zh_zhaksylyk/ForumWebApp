﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using DAL.DTOs;
using DAL.Entities;

namespace DAL.Mappings
{
    public class SurveyProfile : Profile
    {
        public SurveyProfile()
        {
            CreateMap<Survey, SimpleEntityDTO>();
        }
    }
}
