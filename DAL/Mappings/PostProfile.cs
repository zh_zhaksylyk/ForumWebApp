﻿using AutoMapper;
using DAL.DTOs.Post;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Mappings
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostDTO>();
        }
    }
}
