﻿using AutoMapper;
using DAL.DTOs;
using DAL.DTOs.Category;
using DAL.Entities;
using System.Linq;

namespace DAL.Mappings
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryGetDTO>();
            CreateMap<CategoryCreateDTO, Category>();
        }
    }
}
