﻿using AutoMapper;
using DAL.DTOs;
using DAL.DTOs.Community;
using DAL.Entities;

namespace DAL.Mappings
{
    public class CommunityProfile : Profile
    {
        public CommunityProfile()
        {
            CreateMap<Community, SimpleEntityDTO>();
            CreateMap<Community, CommunityDTO>();
        }
    }
}
