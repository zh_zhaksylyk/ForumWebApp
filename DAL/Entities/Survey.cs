﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Survey : BaseModel
    {
        public int PostId { get; set; }
        public Post Post { get; set; }

        public ICollection<Question> Questions { get; set; }
        public ICollection<SurveyParticipant> SurveyParticipants { get; set; }
    }
}
