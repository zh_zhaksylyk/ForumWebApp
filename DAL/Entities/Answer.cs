﻿
namespace DAL.Entities
{
    public class Answer : BaseModel
    {
        public string Text { get; set; }
        public int VoteCount { get; set; }

        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}
