﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Question : BaseModel
    {
        public string Text { get; set; }

        public int SurveyId { get; set; }
        public Survey Survey { get; set; }

        public ICollection<Answer> Answers { get; set; }
    }
}
