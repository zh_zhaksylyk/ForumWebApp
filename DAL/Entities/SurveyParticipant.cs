﻿
namespace DAL.Entities
{
    public class SurveyParticipant
    {
        public int SurveyId { get; set; }
        public Survey Survey { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
