﻿using DAL.DTOs.Survey;

namespace BLL.DTOs
{
    public class SurveyWithResultsForUserDTO : SurveyWithResultsDTO
    {
        public bool CanUserParticipate { get; set; }
    }
}