﻿using BLL.DTOs;
using DAL.DTOs.Survey;

namespace BLL.Interfaces
{
    public interface ISurveyService
    {
        void CreateSurvey(SurveyCreateDTO surveyDto);
        void FillOutSurvey(SurveyFillOutFormDTO form);
        SurveyWithResultsForUserDTO GetSurveyWithResults(int surveyId, int? currentUserId);
    }
}