﻿using DAL.Interfaces;
using AutoMapper;
using System;
using BLL.Exceptions;
using DAL.DTOs.Survey;
using System.Collections;
using System.Linq;
using BLL.Interfaces;
using BLL.DTOs;

namespace BLL.Services
{
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ISurveyResultRepository _surveyResultRepository;
        private readonly IMapper _mapper;

        public SurveyService(ISurveyRepository surveyRepository)
        {
            _surveyRepository = surveyRepository;
        }

        public SurveyService(ISurveyRepository surveyRepository, ISurveyResultRepository surveyResultRepository,
            IMapper mapper)
        {
            _surveyRepository = surveyRepository;
            _surveyResultRepository = surveyResultRepository;
            _mapper = mapper;
        }

        public void CreateSurvey(SurveyCreateDTO surveyDto)
        {
            _surveyRepository.Create(surveyDto);
        }

        public SurveyWithResultsForUserDTO GetSurveyWithResults(int surveyId, int? currentUserId)
        {
            var surveyDto = _surveyRepository.GetWithResults(surveyId);

            foreach (var question in surveyDto.Questions)
            {
                int participantCount = surveyDto.ParticipantCount;
                if (participantCount > 0)
                {
                    foreach (var answer in question.Answers)
                    {
                        answer.VotePercentage = Math.Round((double)answer.VoteCount / participantCount * 100, 2);
                    }
                }
            }

            var surveyWithResultsDto = _mapper.Map<SurveyWithResultsForUserDTO>(surveyDto);

            var isUserParticipant = false;
            if (currentUserId != null)
            {
                isUserParticipant = _surveyResultRepository.DidUserParticipate(surveyId, currentUserId.Value);
            }

            surveyWithResultsDto.CanUserParticipate = 
                currentUserId != null &&
                currentUserId != surveyWithResultsDto.AuthorId &&
                !isUserParticipant;

            return surveyWithResultsDto;
        }

        public void FillOutSurvey(SurveyFillOutFormDTO form)
        {
            var isParticipant = _surveyResultRepository.DidUserParticipate(form.SurveyId, form.UserId);

            if (isParticipant)
            {
                throw new InvalidSurveyFormException($"Already participated in the survey: {form}");
            }

            var survey = _surveyRepository.GetWithResults(form.SurveyId);

            if (survey.Questions.Count() != form.Answers.Count())
            {
                throw new InvalidSurveyFormException($"No answers to all questions: {form}");
            }

            _surveyResultRepository.Update(form);
        }
    }
}