﻿using AutoMapper;
using BLL.DTOs;
using DAL.DTOs;
using DAL.DTOs.Survey;
using DAL.Entities;
using System.Linq;

namespace BLL.Mappings
{
    public class SurveyProfile : Profile
    {
        public SurveyProfile()
        {
            CreateMap<SurveyCreateDTO, Survey>();
            CreateMap<Survey, SurveyWithResultsDTO>()
                .ForMember(dest => dest.SurveyId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ParticipantCount, opt => opt.MapFrom(src => src.SurveyParticipants.Count()))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Post.Title))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Post.Content));
            CreateMap<Question, SurveyWithResultsDTO.Question>()
                .ForMember(dest => dest.QuestionId, opt => opt.MapFrom(src => src.Id));
            CreateMap<Answer, SurveyWithResultsDTO.Answer>()
                .ForMember(dest => dest.AnswerId, opt => opt.MapFrom(src => src.Id));
            CreateMap<SurveyWithResultsDTO, SurveyWithResultsForUserDTO>();
        }
    }
}
