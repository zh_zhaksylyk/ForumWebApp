﻿using System;

namespace BLL.Exceptions
{
    public class InvalidSurveyFormException : Exception
    {
        public InvalidSurveyFormException(string message) : base(message)
        {

        }
    }
}
